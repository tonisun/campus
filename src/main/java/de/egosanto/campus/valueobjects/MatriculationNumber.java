package de.egosanto.campus.valueobjects;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class MatriculationNumber {

    private Long matriculationNumber;
}
