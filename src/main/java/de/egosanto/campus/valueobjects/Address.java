package de.egosanto.campus.valueobjects;

import lombok.*;

import javax.persistence.Embeddable;

@Embeddable
@NoArgsConstructor
@AllArgsConstructor
@Getter
@EqualsAndHashCode
public class Address {
       private String postcode;
       private String city;
       private String street;
       private Integer number;
}
