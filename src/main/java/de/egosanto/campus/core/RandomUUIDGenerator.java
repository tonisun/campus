package de.egosanto.campus.core;

import java.util.UUID;

public class RandomUUIDGenerator {
    public static void main(String[] args) {
        UUID uuid = UUID.randomUUID();
        System.out.println(uuid.toString());
    }
}
