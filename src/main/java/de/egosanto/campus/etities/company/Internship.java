package de.egosanto.campus.etities.company;


import de.egosanto.campus.core.AbstractEntity;
import de.egosanto.campus.etities.student.Student;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Setter
@Getter
@NoArgsConstructor
public class Internship extends AbstractEntity {

    private String startDate;

    private String endDate;

    @OneToOne
    private Student student;

    @ManyToOne
    private Company company;

    @ManyToOne
    private Department departmnet;

    public Internship (UUID id) { super (id);}

    public Internship() {
        super(id);
    }
}
