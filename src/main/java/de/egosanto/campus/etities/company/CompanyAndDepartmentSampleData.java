package de.egosanto.campus.etities.company;

import de.egosanto.campus.etities.student.Student;
import de.egosanto.campus.etities.student.StudentSampleData;
import de.egosanto.campus.repository.company.CompanyRepository;
import de.egosanto.campus.repository.company.DepartmentRepository;
import de.egosanto.campus.repository.student.StudentRepository;
import de.egosanto.campus.valueobjects.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.UUID;

@Component
public class CompanyAndDepartmentSampleData {
    public static final String COMPANY_404_NAME = "404 Soft GmbH";
    public static final UUID COMPANY_404_ID = UUID.fromString("9e2b6f9f-9f49-4778-afc8-bf42678a09f3" );
    public static final String COMPANY_404_BACKEND_DEV = "Back-404";
    public static final String COMPANY_404_WEB_DEV = "Web-404";
    public static final String COMPANY_404_MARKETING = "Mark-404";
    public static final Address COMPANY_404_ADDRESS = new Address("48882", "Ratingen", "Balcke-Dürr-Alle", 87 );

    public static final String COMPANY_SWCHAMPS_NAME = "SoftwareChampsLouser AG & Co. KG";
    public static final UUID COMPANY_SWCHAMPS_ID = UUID.fromString("767a9493-3d5a-4423-8210-00f160be017f");
    public static final String COMPANY_SWCHAMPS_KOELN = "SWCL Köln";
    public static final Address COMPANY_SWCHAMPS_KOELN_ADDRESS = new Address ("50678", "Köln", "Konrad-Adenauer-Ufer", 81 );
    public static final String COMPANY_SWCHAMPS_BERLIN = "SWCL Berlin";
    public static final Address COMPANY_SWCHAMPS_BERLIN_ADDRESS = new Address ( "10557", "Berlin", "Shanse Lise Ave", 4 ) ;
    public static final String COMPANY_SWCHAMPS_HAMBURG = "SWCL Hamburg";
    public static final Address COMPANY_SWCHAMPS_HAMBURG_ADDRESS = new Address ( "20096", "Hamburg", "Reeper Bahn Gasse", 6 );








    private CompanyRepository companyRepository;
    private DepartmentRepository departmentRepository;
    private StudentRepository studentRepository;

    /**
     *
     */
    @Autowired
    public CompanyAndDepartmentSampleData (
            CompanyRepository companyRepository,
            DepartmentRepository departmentRepository,
            StudentRepository studentRepository ) {
        this.companyRepository = companyRepository;
        this.departmentRepository = departmentRepository;
        this.studentRepository = studentRepository;
    }


    /**
     *
     */
    public void createAll() {
        create404();
        createSWChamps();
    }

    /**
     *
     */
    public void create404 () {
        Department department1 = new Department(COMPANY_404_BACKEND_DEV, COMPANY_404_ADDRESS );
        departmentRepository.save(department1);

        Department department2 = new Department(COMPANY_404_WEB_DEV, COMPANY_404_ADDRESS );
        departmentRepository.save(department1);

        Department department3 = new Department(COMPANY_404_MARKETING, COMPANY_404_ADDRESS );
        departmentRepository.save(department1);

        Company company = new Company( COMPANY_404_ID );
        company.setName( COMPANY_404_NAME );

        Optional<Student> studentOptional = studentRepository.findById( StudentSampleData.STUDENT_AMELIE_ID );
        if ( !studentOptional.isEmpty() ) company.getFollowers().add( studentOptional.get() );

        company.getDepartments().add( department1 );
        company.getDepartments().add( department2 );
        company.getDepartments().add( department3 );

        companyRepository.save(company);
    }

    /**
     *
     */
    public void createSWChamps() {
        Department department1 = new Department(COMPANY_SWCHAMPS_KOELN, COMPANY_SWCHAMPS_KOELN_ADDRESS );
        departmentRepository.save(department1);

        Department department2 = new Department(COMPANY_SWCHAMPS_BERLIN, COMPANY_SWCHAMPS_BERLIN_ADDRESS );
        departmentRepository.save(department1);

        Department department3 = new Department(COMPANY_SWCHAMPS_HAMBURG, COMPANY_SWCHAMPS_HAMBURG_ADDRESS );
        departmentRepository.save(department1);

        Company company = new Company( COMPANY_SWCHAMPS_ID );
        company.setName( COMPANY_SWCHAMPS_NAME );

        Optional<Student> studentOptional = studentRepository.findById( StudentSampleData.STUDENT_ANELIE_ID );
        if ( !studentOptional.isEmpty() ) company.getFollowers().add( studentOptional.get() );

        studentOptional = studentRepository.findById( StudentSampleData.STUDENT_GEORG_ID );
        if ( !studentOptional.isEmpty() ) company.getFollowers().add( studentOptional.get() );

        studentOptional = studentRepository.findById( StudentSampleData.STUDENT_MEHMET_ID );
        if ( !studentOptional.isEmpty() ) company.getFollowers().add( studentOptional.get() );

        company.getDepartments().add( department1 );
        company.getDepartments().add( department2 );
        company.getDepartments().add( department3 );

        companyRepository.save(company);
    }
}
