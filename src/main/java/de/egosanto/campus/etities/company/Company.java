package de.egosanto.campus.etities.company;

import de.egosanto.campus.core.AbstractEntity;
import de.egosanto.campus.etities.student.Student;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Setter
@Getter
@NoArgsConstructor
public class Company extends AbstractEntity {

    private String name;

    @ManyToMany
    private final List<Student> followers = new ArrayList<>();

    @OneToMany( cascade = CascadeType.REMOVE)
    private final List<Department> departments = new ArrayList<>();

    public Company ( UUID id ) {
        super( id );
    }

}
