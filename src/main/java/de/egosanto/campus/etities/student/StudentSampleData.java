package de.egosanto.campus.etities.student;

import de.egosanto.campus.valueobjects.Address;
import de.egosanto.campus.valueobjects.MatriculationNumber;
import de.egosanto.campus.repository.student.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class StudentSampleData {

    public static final UUID STUDENT_AMELIE_ID = UUID.fromString ("65628d2f-ec6c-47c2-8646-11d693b3780d");

    @Autowired
    private StudentRepository studentRepository;

    public void createStudents () {
        Student student = new Student ();
        student.setName ("Amelie");
        student.setMatriculationNumber ( new MatriculationNumber (123456789l) );
        student.setAddress ( new Address ("50666","Cologne","Hohe Str.", 6 ) );
        //student. ( new Address ("50888","Cologne","Gosip Girl Str.", 23 ) );
        studentRepository.save (student);
    }

}
