package de.egosanto.campus.etities.student;

import de.egosanto.campus.valueobjects.Address;
import de.egosanto.campus.valueobjects.MatriculationNumber;
import de.egosanto.campus.etities.studyprogram.StudyProgram;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "student")
@Getter
@Setter
@NoArgsConstructor
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Embedded
    private MatriculationNumber matriculationNumber;

    @Embedded
    private Address address;

    @ElementCollection( targetClass = Address.class, fetch = FetchType.EAGER )
    private final List<Address> secondAddresses = new ArrayList<>();

    @Embedded
    private Resume resume;

    @ManyToOne
    private StudyProgram studyProgram;


}
