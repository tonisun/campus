package de.egosanto.campus.repository.company;

import de.egosanto.campus.etities.company.Company;
import de.egosanto.campus.etities.student.Student;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.UUID;

public interface CompanyRepository extends CrudRepository<Company, UUID> {
    public List<Company> findCompaniesByFollowersContaining (Student student);
}
