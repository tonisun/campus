package de.egosanto.campus.repository.company;

import de.egosanto.campus.etities.company.Department;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface DepartmentRepository extends CrudRepository<Department, UUID> {

}
