package de.egosanto.campus.repository.student;

import de.egosanto.campus.etities.student.Student;
import org.springframework.data.repository.CrudRepository;

public interface StudentRepository extends CrudRepository<Student, Long> {

}
