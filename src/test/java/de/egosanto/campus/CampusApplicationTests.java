package de.egosanto.campus;

import de.egosanto.campus.valueobjects.MatriculationNumber;
import de.egosanto.campus.etities.student.Student;
import de.egosanto.campus.etities.student.StudentSampleData;
import de.egosanto.campus.repository.student.StudentRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

@SpringBootTest
class CampusApplicationTests {

	@Autowired
	private StudentRepository studentRepository;

	@Autowired
	private StudentSampleData studentSampleData;

	@BeforeEach
	public void  setupData () {
		studentSampleData.createStudents();
	}

	@AfterEach
	public void tearDown () {
		studentRepository.deleteAll();
	}

	@Test
	public void testHasStudentTheNameAmelieAndHasCorrMatriculationNumber () {
		Optional<Student> studentOptional = studentRepository.findById(1l);

		assertFalse(studentOptional.isEmpty());
		Student amelie = studentOptional.get();

		assertEquals("Amelie", amelie.getName());

		assertEquals(new MatriculationNumber(123456789l), amelie.getMatriculationNumber());
	}

}
