package de.egosanto.campus.valueobjects;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;


import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest
public class AddressTests {

       @Test
       public void testTwoAddressesEqual() {
              Address address1 = new Address( "41179", "Mönchengladbach", "Hennes-Weisweiler-Allee", 666 );
              Address address2 = new Address( "41179", "Mönchengladbach", "Hennes-Weisweiler-Allee", 666 );

              assertEquals(address1, address2);
       }
}
